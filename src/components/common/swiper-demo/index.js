import SwiperDemo from './SwiperDemo'
import SwiperItemDemo from './SwiperItemDemo'

export {
  SwiperDemo, SwiperItemDemo
}